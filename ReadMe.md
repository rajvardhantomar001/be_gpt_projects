# Setup

1. Fork this repo. Clone your fork locally.
2. Setup openai, langchain and flask in your local environment.
3. `pip install --upgrade openai`
4. 'pip install langchain' in your cmd.
5. 'pip install flask'
6. Run app.py file to run the flask applicaion.
7. Make sure you have a database for storing the flask applicaion details.